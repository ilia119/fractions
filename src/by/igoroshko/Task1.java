package by.igoroshko;

public class Task1 {
    public static void main(String[] args) {
        new Task1().runTheApp();
    }

    private void runTheApp() {
        Fraction f1 = new Fraction(3, 3);
        Fraction f2 = new Fraction(5, 10);
        Fraction f3 = f1.add(f2);
        Fraction f4 = f1.multiply(f2);
        System.out.println(f3.toString());
        System.out.println(f4.toString());

        /*int [] arr = new int[] {1, 2, 3, 4, 6, 8, 7, 9};
        System.out.println(method2(-2, arr));*/


    }

    public int method1(int a) {
        return a*a;
    }

    public int method2(int i, int[] arr){
        int realIndex = i < 0 ? arr.length + i : i;
        if(i > arr.length)
            throw new ArrayIndexOutOfBoundsException();
        return arr[realIndex];

    }




}
